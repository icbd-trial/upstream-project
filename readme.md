# Readme

This is a Demo project to show new code sync flow.

```
gitlab.com/gitlab-org/gitlab: master

	 ||
	 || auto mirror
	 || 
	 \/

jihulab.com/gitlab-cn/gitlab: master

	||
	|| auto scheduled code sync (merge and bunlde)
	||
	\/

jihulab.com/gitlab-cn/gitlab: pre-main-jh

	||
	|| fix changes
	||
	\/

jihulab.com/gitlab-cn/gitlab: pre-main-jh

	||
	|| auto merge
	||
	\/
jihulab.com/gitlab-cn/gitlab: main-jh
```

```

master:
+---------+
| Commits |  --->  C1     --->  C2
+---------+                     |
                                |
                            scheduled
pre-main-jh:                    |
+---------+                     v
| Commits |  --->  C1(√)  --->  C2(X)  -->  F2(√)
+---------+                                 |
                                            |
                                            |triger
main-jh:                                    |
+---------+                                 v
| Commits |  --->  C1(√)  --------------->  C2F2(√)
+---------+
```
